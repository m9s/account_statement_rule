#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:account.statement.rule,amount_high:"
msgid "Amount High"
msgstr ""

msgctxt "field:account.statement.rule,amount_low:"
msgid "Amount Low"
msgstr ""

msgctxt "field:account.statement.rule,company:"
msgid "Company"
msgstr ""

msgctxt "field:account.statement.rule,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:account.statement.rule,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:account.statement.rule,currency_digits:"
msgid "Currency Digits"
msgstr ""

msgctxt "field:account.statement.rule,description:"
msgid "Description"
msgstr ""

msgctxt "field:account.statement.rule,id:"
msgid "ID"
msgstr ""

msgctxt "field:account.statement.rule,information_rules:"
msgid "Information Rules"
msgstr ""

msgctxt "field:account.statement.rule,journal:"
msgid "Journal"
msgstr ""

msgctxt "field:account.statement.rule,lines:"
msgid "Lines"
msgstr ""

msgctxt "field:account.statement.rule,name:"
msgid "Name"
msgstr ""

msgctxt "field:account.statement.rule,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:account.statement.rule,sequence:"
msgid "Sequence"
msgstr ""

msgctxt "field:account.statement.rule,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:account.statement.rule,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:account.statement.rule.information,boolean:"
msgid "Boolean"
msgstr ""

msgctxt "field:account.statement.rule.information,char:"
msgid "Char"
msgstr ""

msgctxt "field:account.statement.rule.information,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:account.statement.rule.information,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:account.statement.rule.information,float_high:"
msgid "Float High"
msgstr ""

msgctxt "field:account.statement.rule.information,float_low:"
msgid "Float Low"
msgstr ""

msgctxt "field:account.statement.rule.information,id:"
msgid "ID"
msgstr ""

msgctxt "field:account.statement.rule.information,integer_high:"
msgid "Integer High"
msgstr ""

msgctxt "field:account.statement.rule.information,integer_low:"
msgid "Integer Low"
msgstr ""

msgctxt "field:account.statement.rule.information,key:"
msgid "Key"
msgstr ""

msgctxt "field:account.statement.rule.information,key_type:"
msgid "Key Type"
msgstr ""

msgctxt "field:account.statement.rule.information,number_high:"
msgid "Numeric High"
msgstr ""

msgctxt "field:account.statement.rule.information,number_low:"
msgid "Numeric Low"
msgstr ""

msgctxt "field:account.statement.rule.information,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:account.statement.rule.information,rule:"
msgid "Rule"
msgstr ""

msgctxt "field:account.statement.rule.information,selection:"
msgid "Selection"
msgstr ""

msgctxt "field:account.statement.rule.information,sequence:"
msgid "Sequence"
msgstr ""

msgctxt "field:account.statement.rule.information,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:account.statement.rule.information,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:account.statement.rule.line,account:"
msgid "Account"
msgstr ""

msgctxt "field:account.statement.rule.line,amount:"
msgid "Amount"
msgstr ""

msgctxt "field:account.statement.rule.line,company:"
msgid "Company"
msgstr ""

msgctxt "field:account.statement.rule.line,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:account.statement.rule.line,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:account.statement.rule.line,id:"
msgid "ID"
msgstr ""

msgctxt "field:account.statement.rule.line,party:"
msgid "Party"
msgstr ""

msgctxt "field:account.statement.rule.line,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:account.statement.rule.line,rule:"
msgid "Rule"
msgstr ""

msgctxt "field:account.statement.rule.line,sequence:"
msgid "Sequence"
msgstr ""

msgctxt "field:account.statement.rule.line,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:account.statement.rule.line,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "help:account.statement.rule,description:"
msgid ""
"The regular expression the description is searched with.\n"
"It may define the groups named:\n"
"'party', 'bank_account', 'invoice'."
msgstr ""

msgctxt "help:account.statement.rule.information,char:"
msgid ""
"The regular expression the key information is searched with.\n"
"It may define the groups named:\n"
"party, bank_account, invoice."
msgstr ""

msgctxt "help:account.statement.rule.line,account:"
msgid ""
"Leave empty to use the party's receivable or payable account.\n"
"The rule must have a company to use this field."
msgstr ""

msgctxt "help:account.statement.rule.line,amount:"
msgid "A Python expression evaluated with 'amount' and 'pending'."
msgstr ""

msgctxt "help:account.statement.rule.line,party:"
msgid ""
"Leave empty to use the group named 'party' from the regular expressions."
msgstr ""

msgctxt "model:account.statement.rule,name:"
msgid "Account Statement Rule"
msgstr ""

msgctxt "model:account.statement.rule.information,name:"
msgid "Account Statement Rule Information"
msgstr ""

msgctxt "model:account.statement.rule.line,name:"
msgid "Account Statement Rule Line"
msgstr ""

msgctxt "model:ir.action,name:act_rule_form"
msgid "Rules"
msgstr ""

msgctxt "model:ir.model.button,string:statement_apply_rules_button"
msgid "Apply Rules"
msgstr ""

msgctxt "model:ir.ui.menu,name:menu_rule_form"
msgid "Rules"
msgstr ""

#, fuzzy
msgctxt "view:account.statement.rule.information:"
msgid "-"
msgstr "-"

#, fuzzy
msgctxt "view:account.statement.rule:"
msgid "-"
msgstr "-"

msgctxt "view:account.statement.rule:"
msgid "Between"
msgstr ""

msgctxt "view:account.statement.rule:"
msgid "Criteria"
msgstr ""
